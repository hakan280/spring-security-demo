package com.linovi.hakan.springsecuritydemo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HomeController {

    @GetMapping
    public String welcomeHome() {
        return "Welcome Home : )";
    }

    @GetMapping("/simple")
    public String simpleUser() {
        return "Welcome secured simple page USER ROLE-> ";
    }

    @GetMapping("/simple/admin")
    public String simpleAmin() {
        return "Welcome secured simple page ADMIN ROLE -> ";
    }

}
